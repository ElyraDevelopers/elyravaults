package com.elyriacraft.plugins.ElyriaVaults.storage;

import com.ataxiaserver.plugins.AtaxiaLib.plugin.MainClassTemplate;
import com.ataxiaserver.plugins.AtaxiaLib.plugin.storage.CustomFileManager;
import com.ataxiaserver.plugins.AtaxiaLib.plugin.storage.StorageManagerTemplate;
public class StorageManager extends StorageManagerTemplate{

	public StorageManager(MainClassTemplate plugin) {
		super(plugin);
		vaultFile = new CustomFileManager(plugin, "vaults");
	}
	private CustomFileManager vaultFile;

	@Override
	public void reloadAll() {	
		vaultFile.reloadConfig();
	}
	public CustomFileManager getVaultFile(){
		return vaultFile;
	}


}
