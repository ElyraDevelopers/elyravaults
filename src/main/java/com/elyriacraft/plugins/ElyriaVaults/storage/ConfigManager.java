package com.elyriacraft.plugins.ElyriaVaults.storage;

import org.bukkit.configuration.file.FileConfiguration;

import com.ataxiaserver.plugins.AtaxiaLib.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaVaults.EvMainClass;

public enum ConfigManager{
	
	VAULT_ITEM_ID("vault item.id", "34"),
	VAULT_ITEM_DATA("vault item.data", "0"),
	VAULT_BLOCK_ID("vault block.id", "33"),
	VAULT_BLOCK_DATA("vault block.data", "6");

	private String string = null;
	private Object defaultValue = null;
	private MainClassTemplate plugin = EvMainClass.getInstance();
	private FileConfiguration co = plugin.getConfig();
	
	private ConfigManager(String config, String defaultValue){
		this.string = config;
		this.defaultValue = defaultValue;
		co.addDefault(string, defaultValue);	
		}

	public String get() {
		return co.getString(string);
	}

	public void set(String value){
	co.set(string, value);
	plugin.saveConfig();
	}

	public void addDefault() {
		co.addDefault(string, defaultValue);		
	}
	/**
	 * adds all the defaults defined in the ConfigManager enumerator
	 */
	public final static void doConfigDefaults(){
		ConfigManager[] confValues = ConfigManager.values();
		for(ConfigManager confVal : confValues){
			confVal.addDefault();
		}
		EvMainClass.getInstance().getConfig().options().copyDefaults(true);
		EvMainClass.getInstance().saveConfig();
	}

}
