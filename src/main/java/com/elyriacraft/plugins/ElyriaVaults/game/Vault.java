package com.elyriacraft.plugins.ElyriaVaults.game;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.Inventory;

import com.ataxiaserver.plugins.AtaxiaLib.utils.InventorySerializer;
import com.ataxiaserver.plugins.AtaxiaLib.utils.InventoryUtils;
import com.ataxiaserver.plugins.AtaxiaLib.utils.LocationUtils;
import com.elyriacraft.plugins.ElyriaVaults.EvMainClass;

public class Vault {
	
	private EvMainClass plugin = EvMainClass.getInstance();
	private Location loc;
	public Vault(Location loc){
		this.loc = loc;
	}
	private FileConfiguration c = plugin.getStorageManager().getVaultFile().getConfig();
	private ConfigurationSection cs = c.createSection(LocationUtils.toBlockString(loc));
	public void addNewVault(String title, String ownerName, String... friendsNames){
		 Inventory inv = Bukkit.createInventory(null, 54, title);
		 List<String> invstring = InventorySerializer.toString(inv);
		 cs.set("title", title);
		 cs.set("owner", ownerName);
		 cs.set("friends", friendsNames);
		 cs.set("inventory", invstring);
	}
	public Inventory getInv(){
		if(!exists()){
			return null;
		}
		List<String> stringItems = cs.getStringList("inventory");
		Inventory inv = InventoryUtils.toInventory(stringItems, cs.getString("title"));
		return inv;
	}
	public boolean exists(){
		if(cs == null){
			return false;
		}
		return true;
	}
}
