package com.elyriacraft.plugins.ElyriaVaults.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import com.ataxiaserver.plugins.AtaxiaLib.plugin.PluginLogger;
import com.elyriacraft.plugins.ElyriaVaults.EvMainClass;
import com.elyriacraft.plugins.ElyriaVaults.game.Vault;
import com.elyriacraft.plugins.ElyriaVaults.storage.ConfigManager;

public class InteractEvent implements Listener{
	
	private int bId = Integer.parseInt(ConfigManager.VAULT_BLOCK_ID.get());
	private byte bData = Byte.parseByte((String)ConfigManager.VAULT_BLOCK_DATA.get());
	private PluginLogger log = EvMainClass.getInstance().getLog();
	
	@EventHandler(priority = EventPriority.HIGH)
	public void openInventory(PlayerInteractEvent event){
		Player p = event.getPlayer();
		Block b = event.getClickedBlock();
		if(b.getTypeId() == bId && b.getData() == bData){
			if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
				log.debug("InteractEvent.onPlayerInteract() - player '" + p.getName() + "' right clicked a Vault");
				Vault vault = new Vault(b.getLocation());
				Inventory inv = vault.getInv();
				if(inv == null){
					vault.addNewVault("Vault", p.getName());
					inv = vault.getInv();
				}
				p.openInventory(inv);
			}
		}
		
	}
	@EventHandler(priority = EventPriority.NORMAL)
	public void placeVault(BlockPlaceEvent event){
		Player p = event.getPlayer();
		int iId = Integer.parseInt(ConfigManager.VAULT_ITEM_ID.get());
		byte iData = Byte.parseByte((String)ConfigManager.VAULT_ITEM_DATA.get());
		if(event.getBlock().getTypeId() == iId && event.getBlock().getData() == iData){
			log.debug("InteractEvent.placeVault() - player " + p.getName() + " placed a vault!");
			event.setCancelled(false);
		}
	}
}
