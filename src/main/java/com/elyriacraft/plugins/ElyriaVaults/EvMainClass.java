package com.elyriacraft.plugins.ElyriaVaults;

import com.ataxiaserver.plugins.AtaxiaLib.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaVaults.events.InteractEvent;
import com.elyriacraft.plugins.ElyriaVaults.storage.ConfigManager;
import com.elyriacraft.plugins.ElyriaVaults.storage.StorageManager;

public class EvMainClass extends MainClassTemplate {

	private static EvMainClass instance;{
		instance = this;
	}
	public static EvMainClass getInstance(){
		return instance;
	}
	@Override
	public void onEnable(){
		super.storageMan = new StorageManager(this);
		getServer().getPluginManager().registerEvents(new InteractEvent(), this);
		getConfig().addDefault("Debug", false);
		getConfig().options().copyDefaults(true);
		ConfigManager.doConfigDefaults();
	}
	@Override
	public void onDisable(){
		saveConfig();
		((StorageManager) storageMan).getVaultFile().saveFile();
	}
	@Override
	public StorageManager getStorageManager(){
		return (StorageManager) super.storageMan;
		
	}
	public String getPrefix(){
		return "ElyriaVaults";
	}
	
	
}